import './App.css';
import { Component } from 'react';

 class App extends Component {
  // locale state  only available inside this component

  constructor(){
    super();

    // instantiate the state
    this.state = {
        monsters :[],
        searchFlied: ''
    }
    console.log("constructor");
  }

  // Lifecycle method

  // when to mount value
  componentDidMount(){
    console.log("componentDidMount");
    // when the component is first mounted
    fetch('https://jsonplaceholder.typicode.com/users')
      .then( response => response.json())
      .then((users) => this.setState(
        () => {
          return {monsters:users};
        }, 
        () => {
          console.log(this.state)
        })
      );
  }

  // how to load value

  render() {
    console.log("render");

    const filteredMonsters = this.state.monsters.filter((monster)=>{
      return monster.name.toLowerCase().includes(this.state.searchFlied)
  });

    return (      
      <div className="App">
        <input className='search-box' type='search' placeholder='search by name' 
          onChange={
            (event) =>{

              const searchFlied = event.target.value.toLowerCase();

              this.setState(() => {
                return {searchFlied};                
              });
            }}
        / >
        {/* Using map to display array of values use key  */}
        {
          filteredMonsters.map((monster) => {
            return <div key={monster.id}  >
                <h1>{monster.name}</h1>
            </div>
          })
        }
      </div>
    );
  }
}

export default App;
