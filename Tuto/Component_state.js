import logo from './logo.svg';
import './App.css';
import { Component } from 'react';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

 class App extends Component {
  // locale state  only available inside this component

  constructor(){
    super();

    // instantiate the state
    this.state = {
      name:{
        firstname:"Romeo",
        lastname:"Kamgo"
      },
      age:30
    }

  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Hi {this.state.name.firstname} {this.state.name.lastname} I am {this.state.age} old
          </p>
          {/* eventhandler onclick setState apply a shawdow merge of object */}
          <button 
            onClick={()=>{
              this.setState(() => {
                return{
                  name: {firstname:"LAndry",lastname:"Chetchom"},
                  }
              },
              () => {
                console.log(this.state)
              });
            }}
          > change name </button>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
